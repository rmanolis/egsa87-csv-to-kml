#!/usr/bin/env node
import cli from "commander";
import chalk from "chalk";
import figlet from "figlet";
import { promises as fs, exists as fsExists } from "fs";
import util from "util";
import { fromCsvToKml, fromCsvFolderToKml } from "./logic";
const exists = util.promisify(fsExists);
cli
  .version("0.0.1")
  .description("Transforms CSV files from EGSA87 to KML")
  .option("-c, --csv <type>", "csv file as input")
  .option(
    "-k, --kml <type>",
    "kml file as output, if it is empty will use the name of the csv file"
  )
  .option(
    "-f, --csv-folder <type>",
    "csv folder as input to create one kml file"
  );

cli.parse(process.argv);
async function main() {
  console.log(
    chalk.blue(
      figlet.textSync("EGSA87-CSV-to-KML", { horizontalLayout: "full" })
    )
  );
  const csvFile = cli.csv;
  const csvFolder = cli.csvFolder;
  let kmlFile = cli.kml;
  if (!csvFolder) {
    if (!csvFile) {
      console.log(chalk.red("Error: The CSV file is missing."));
      process.exit(1);
    } else {
      if (!(await exists(csvFile))) {
        console.log(chalk.red("Error: The CSV file does not exists."));
        process.exit(1);
      }
    }
  } else {
    if (!(await exists(csvFolder))) {
      console.log(chalk.red("Error: The CSV folder does not exists."));
      process.exit(1);
    }
    if (!(await fs.lstat(csvFolder)).isDirectory()) {
      console.log(chalk.red("Error: The CSV folder is not a directory."));
      process.exit(1);
    }
  }
  if (!kmlFile) {
    console.log(chalk.red("Error: The KML file is missing."));
    process.exit(1);
  }

  if (csvFile && kmlFile) {
    fromCsvToKml(csvFile, kmlFile);
    process.exit(0);
  }

  if (csvFolder && kmlFile) {
    fromCsvFolderToKml(csvFolder, kmlFile);
  }
}

main();
