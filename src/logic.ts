import csv from "csv-parser";
import { promises as fs, createReadStream } from "fs";

export enum GreekPointAttributes {
  SHMEIO = "Σημείο:",
  PLATOS = " Πλάτος:",
  MIKOS = " Μήκος:",
  IPSOMETRO = " Υψόμετρο:",
  X = " X:",
  Y = " Y:"
}
export interface GreekPoint {
  "Σημείο:": string;
  " Πλάτος:": string;
  " Μήκος:": string;
  " Υψόμετρο:": string;
  " X:": string;
  " Y:": string;
}

export function getCsvList(file: string): Promise<GreekPoint[]> {
  return new Promise((resolve, reject) => {
    const results: GreekPoint[] = [];
    try {
      createReadStream(file)
        .pipe(csv())
        .on("data", data => results.push(data))
        .on("end", () => {
          resolve(results);
        });
    } catch (e) {
      reject(e);
    }
  });
}

export function trimCsvValues(input: GreekPoint[]): GreekPoint[] {
  return input.map(d => {
    const item: GreekPoint = {
      "Σημείο:": d["Σημείο:"].trim(),
      " Υψόμετρο:": d[" Υψόμετρο:"].trim(),
      " Πλάτος:": d[" Πλάτος:"].trim(),
      " Μήκος:": d[" Μήκος:"].trim(),
      " Y:": d[" Y:"].trim(),
      " X:": d[" X:"].trim()
    };
    return item;
  });
}

/*
{
    'Σημείο:': '01 baranena 1',
    ' Υψόμετρο:': '369.82',
    ' Πλάτος:': '35.21156554',
    ' Μήκος:': '25.20627509',
    ' Y:': '3896882.617',
    ' X:': '609644.026'
  },
  <Placemark>+eol		<name>01 baranena 1</name>
		<description>X=609644.026
Y=3896882.617
h=369.82</description>
		<styleUrl>#myPoint</styleUrl>
		<Point>
			<coordinates>25.20627509,35.21156554,390.29</coordinates>
		</Point>
	</Placemark>
*/

export function gpFromJsonToXml(input: GreekPoint): string {
  return `<Placemark>
  <name>${input["Σημείο:"]}</name>
  <description>X=${input[" X:"]}
  Y=${input[" Y:"]}
  h=${input[" Υψόμετρο:"]}</description>
  <styleUrl>#myPoint</styleUrl>
  <Point>
			<coordinates>${input[" Μήκος:"]},
      ${input[" Πλάτος:"]},
      ${input[" Υψόμετρο:"]}</coordinates>
  </Point>
  </Placemark>`;
}

export async function fromCsvToKml(csvFile: string, kmlFile: string) {
  const rawPoints = await getCsvList(csvFile);
  const points = trimCsvValues(rawPoints);
  let xmlPoints = "";
  for (const point of points) {
    xmlPoints += gpFromJsonToXml(point) + "\n";
  }
  const kml = `<?xml version="1.0" encoding="UTF-8"?>
  <kml xmlns="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
  <Document>
    <name>20-10-2019.kml</name>
    <atom:author><atom:name>StgrDev - ΕΓΣΑ87 (GGRS87) coords</atom:name></atom:author>
    <address>stgrdev@gmail.com</address>
    <Style id="myPoint">
      <IconStyle>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>
        </Icon>
      </IconStyle>
    </Style>
    ${xmlPoints}
    </Document>
    </kml>
  `;
  await fs.writeFile(kmlFile, kml);
}

export async function fromCsvFolderToKml(csvFolder: string, kmlFile: string) {
  const files = await fs.readdir(csvFolder);
  const allPoints: Map<string, GreekPoint> = new Map();
  for (const file of files) {
    const csvFile = csvFolder + "/" + file;
    const rawPoints = await getCsvList(csvFile);
    const points = trimCsvValues(rawPoints);
    for (const point of points) {
      if (!allPoints.has(point["Σημείο:"])) {
        allPoints.set(point["Σημείο:"], point);
      }
    }
  }
  let xmlPoints = "";
  allPoints.forEach(point => {
    xmlPoints += gpFromJsonToXml(point) + "\n";
  });
  const kml = `<?xml version="1.0" encoding="UTF-8"?>
  <kml xmlns="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
  <Document>
    <name>20-10-2019.kml</name>
    <atom:author><atom:name>StgrDev - ΕΓΣΑ87 (GGRS87) coords</atom:name></atom:author>
    <address>stgrdev@gmail.com</address>
    <Style id="myPoint">
      <IconStyle>
        <Icon>
          <href>http://maps.google.com/mapfiles/kml/shapes/placemark_circle.png</href>
        </Icon>
      </IconStyle>
    </Style>
    ${xmlPoints}
    </Document>
    </kml>
  `;
  await fs.writeFile(kmlFile, kml);
}
